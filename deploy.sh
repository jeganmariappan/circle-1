#!/bin/bash
mkdir /home/circleci/.aws
touch /home/circleci/.aws/config
chmod 600 /home/circleci/.aws/config
touch /home/circleci/.ssh/id_rsa.pub
echo "[profile user]" > /home/circleci/.aws/config
echo "aws_access_key_id=$AWS_ACCESS_KEY_ID" >> /home/circleci/.aws/config
echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> /home/circleci/.aws/config
echo "default_region_name=$DEFAULT_REGION_NAME" >> /home/circleci/.aws/config
echo "$public_key" >> /home/circleci/.ssh/id_rsa.pub
echo "Start deploy"
ssh -t -o "StrictHostKeyChecking no" ubuntu@34.228.30.234 <<-'ENDSSH'
      ls
      pwd
      git clone https://jeganmariappan@bitbucket.org/jeganmariappan/circle-1.git
      cd circle-1
      npm i
      node start index.js
      echo "Deploy end"
ENDSSH